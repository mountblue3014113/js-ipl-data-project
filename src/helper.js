const csvtojson = require("csvtojson");

function csvTOJSON(path){
    return csvtojson().fromFile(path).then((data)=>{
        return data;
    })
}
module.exports = csvTOJSON;