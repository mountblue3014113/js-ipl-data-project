const fs = require("fs");
const csvTOJSON = require("../helper.js");

const matchesFilepath = "../data/matches.csv";

function matchesPerYear() {
  try {
    csvTOJSON(matchesFilepath).then((matchesDetail) => {
      let yearResult = {};

      for (let index = 0; index < matchesDetail.length; index++) {
        if (!yearResult[matchesDetail[index].season]) {
          yearResult[matchesDetail[index].season] = 1;
        } else {
          yearResult[matchesDetail[index].season]++;
        }
      }

      fs.writeFileSync(
        "../public/output/matchesPerYear.json",
        JSON.stringify(yearResult, null, 2),
        (err) => {
          if (err) {
            console.log(err, "Error writing the file");
          } else {
            console.log("Result written in the output");
          }
        }
      );
    });
  } catch (error) {
    console.log(error);
  }
}
matchesPerYear();
