const csvTOJSON = require("../helper.js");
const fs = require("fs");

const deliveriesFilepath = "../data/deliveries.csv";

  function bestEconomyInSuperOver() {
    try {
    csvTOJSON(deliveriesFilepath).then(function (deliveriesData) {
      let superOverData = [];

      for (let index = 0; index < deliveriesData.length; index++) {
        if (deliveriesData[index].is_super_over === "1") {
          superOverData.push(deliveriesData[index]);
        }
      }

      let economyRates = {};

      for (let index = 0; index < superOverData.length; index++) {
        let delivery = superOverData[index];
        let bowler = delivery.bowler;
        let totalRuns =
          Number(delivery.batsman_runs) +
          Number(delivery.wide_runs) +
          Number(delivery.noball_runs);

        if (!economyRates[bowler]) {
          economyRates[bowler] = {
            runs: 0,
            balls: 0,
          };
        }
        economyRates[bowler].runs += totalRuns;
        if (delivery.wide_runs == 0 && delivery.noball_runs == 0) {
          economyRates[bowler].balls++;
        }
      }

      let bestEconomyBowler = {
        bowler: "",
        economyRate: 0,
      };
      let bestEconomyRate = Infinity;
      for (let bowler in economyRates) {
        let runs = economyRates[bowler].runs;
        let balls = economyRates[bowler].balls;
        let economyRate = (runs / balls) * 6;
        economyRate = Number(economyRate);

        if (economyRate < bestEconomyRate) {
          bestEconomyBowler = bowler;
          bestEconomyRate = economyRate;
        }
      }

      fs.writeFileSync(
        "../public/output/bestEconomyInSuperOvers.json",
        JSON.stringify(
          { bowler: bestEconomyBowler, economyRate: bestEconomyRate },
          null,
          2
        ),
        function (err) {
          if (err) {
            console.log(err, "Error writing the file");
          } else {
            console.log("Result written in the output");
          }
        }
      );
    });
} catch (error) {
    console.log("Error");
  }
  }

bestEconomyInSuperOver();
