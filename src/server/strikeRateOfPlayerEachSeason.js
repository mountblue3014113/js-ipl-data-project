const { CLIENT_RENEG_WINDOW } = require("tls");
const csvTOJSON = require("../helper.js");
const fs = require("fs");

const matchesFilepath = "../data/matches.csv";
const deliveriesFilepath = "../data/deliveries.csv";

function calculateStrikeRatePerSeason() {
    try{
  csvTOJSON(deliveriesFilepath).then(function (deliveriesData) {
    csvTOJSON(matchesFilepath).then(function (matchData) {
      let strikeRatePerSeason = {};

      for (let index = 0; index < deliveriesData.length; index++) {
        let delivery = deliveriesData[index];
        let batsman = delivery.batsman;
        let season = matchData.find(
          (match) => match.id === delivery.match_id
        ).season;
        let runs = Number(delivery.batsman_runs);
        let isWide = Number(delivery.wide_runs) !== 0;
        let isNoBall = Number(delivery.noball_runs) !== 0;

        if (!isWide && !isNoBall) {
          if (!strikeRatePerSeason[season]) {
            strikeRatePerSeason[season] = {};
          }

          if (!strikeRatePerSeason[season][batsman]) {
            strikeRatePerSeason[season][batsman] = {
              runs: 0,
              balls: 0,
            };
          }

          strikeRatePerSeason[season][batsman].runs += runs;
          strikeRatePerSeason[season][batsman].balls++;
        }
      }

      for (let season in strikeRatePerSeason) {
        for (let batsman in strikeRatePerSeason[season]) {
          let batsmanData = strikeRatePerSeason[season][batsman];
          let strikeRate = (batsmanData.runs / batsmanData.balls) * 100;
          batsmanData.strikeRate = strikeRate;
        }
      }

      fs.writeFileSync(
        "../public/output/strikeRateOfPlayerEachSeason.json",
        JSON.stringify(strikeRatePerSeason, null, 2),
        function (err) {
          if (err) {
            console.log(err, "Error writing the file");
          } else {
            console.log("Result written in the output");
          }
        }
      );
    });
  });
}catch(error){
    console.log(error);
}
}
calculateStrikeRatePerSeason();
