const csvTOJSON = require("../helper.js");
const fs = require("fs");

const deliveriesFilepath = "../data/deliveries.csv";

function HighestTimesDismissedByAnyPlayer() {
    try{
    csvTOJSON(deliveriesFilepath).then(function(deliveriesData) {
        let dismissals = {};

        for (let index = 0; index < deliveriesData.length; index++) {
            let delivery = deliveriesData[index];
            let dismissedPlayer = delivery.player_dismissed;
            let dismissalKind = delivery.dismissal_kind;
            let bowler = delivery.bowler;

            if (dismissedPlayer && dismissalKind !== 'run out') {
                if (!dismissals[dismissedPlayer]) {
                    dismissals[dismissedPlayer] = {};
                }

                if (!dismissals[dismissedPlayer][bowler]) {
                    dismissals[dismissedPlayer][bowler] = 1;
                } else {
                    dismissals[dismissedPlayer][bowler]++;
                }
            }
        }

        let result = [];

        
        //let dismissedPlayer;

        for (let player in dismissals) {
            let highestDismissals = {
                dismissedPlayer: '',
                bowler: '',
                count: 0
            };
            for (let dismissedBy in dismissals[player]) {
                let count = dismissals[player][dismissedBy];
                if (count > highestDismissals.count) {
                    highestDismissals.dismissedPlayer = player;
                    highestDismissals.bowler = dismissedBy;
                    highestDismissals.count = count;
                }
            }
            result.push(highestDismissals);
        }

        fs.writeFileSync("../public/output/HighestTimesDismissedByAnyPlayer.json", JSON.stringify(result, null, 2), function(err) {
            if (err) {
                console.log(err, "Error writing the file");
            } else {
                console.log("Result written in the output");
            }
        });
    });
}catch(error){
    console.log(error);
}
}

HighestTimesDismissedByAnyPlayer()