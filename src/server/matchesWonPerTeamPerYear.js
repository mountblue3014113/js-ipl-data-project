const fs = require("fs");
const csvTOJSON = require("../helper.js");

const matchesFilepath = "../data/matches.csv";

function matchesWonPerTeamPerYear() {
  csvTOJSON(matchesFilepath).then((matchesDetail) => {
    let matchesWonPerTeamPerYear = {};

    try {
      for (let index = 0; index < matchesDetail.length; index++) {
        let match = matchesDetail[index];
        let year = match.season;
        let winner = match.winner;

        if (!matchesWonPerTeamPerYear[winner]) {
          matchesWonPerTeamPerYear[winner] = {};
        }
        if (!matchesWonPerTeamPerYear[winner][year]) {
          matchesWonPerTeamPerYear[winner][year] = 1;
        } else {
          matchesWonPerTeamPerYear[winner][year]++;
        }
      }
    } catch (error) {
      console.log("Error");
    }
    fs.writeFileSync(
      "../public/output/matchesWonPerTeamPerYear.json",
      JSON.stringify(matchesWonPerTeamPerYear, null, 2),
      (err) => {
        if (err) {
          console.log(err, "Error writing the file");
        } else {
          console.log("Result written in the output");
        }
      }
    );
  });
}
matchesWonPerTeamPerYear();
