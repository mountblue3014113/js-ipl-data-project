const csvTOJSON =require("../helper.js");
const fs = require("fs");

const matchesFilepath = "../data/matches.csv";

function highestTimePOTMAwardEachSeason() {
    try{
    csvTOJSON(matchesFilepath).then((matchData) => {
        let potmPerSeason = {};

        for (let index = 0; index < matchData.length; index++) {
            let season = matchData[index].season;
            let potm = matchData[index].player_of_match;

            if (potm) {
                if (!potmPerSeason[season]) {
                    potmPerSeason[season] = {};
                }

                if (!potmPerSeason[season][potm]) {
                    potmPerSeason[season][potm] = 1;
                } else {
                    potmPerSeason[season][potm]++;
                }
            }
        }

        let highestPOTMPerSeason ={};
        for(let season in potmPerSeason){
            let highestPOTM = 0;
            let topPOTM;

            for(let potm in potmPerSeason[season]){
                if(potmPerSeason[season][potm]>highestPOTM){
                    highestPOTM = potmPerSeason[season][potm];
                    topPOTM = potm;
                }
            }

            highestPOTMPerSeason[season]={
                player: topPOTM,
                count: highestPOTM
            };
        }

        fs.writeFileSync("../public/output/highestTimePOTMAwardEachSeason.json", JSON.stringify(highestPOTMPerSeason, null, 2),
            (err) => {
                if (err) {
                    console.log(err, "Error !");
                } else {
                    console.log("Result written in the output");
                }
            }
        );
    });
}catch(error){
    console.log(error);
}
}
highestTimePOTMAwardEachSeason();