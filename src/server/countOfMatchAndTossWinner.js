const csvTOJSON =require("../helper.js");
const fs = require("fs");

const matchesFilepath = "../data/matches.csv";
function countOfTossAndMatchWinners() {
    try{
    csvTOJSON(matchesFilepath).then((matchData) => {
        let tossAndMatchWinners = {};
        for (let index = 0; index < matchData.length; index++) {
            let tossWinner = matchData[index].toss_winner;
            let matchWinner = matchData[index].winner;
            if (tossWinner && matchWinner && tossWinner === matchWinner) {
                if (tossAndMatchWinners[tossWinner]) {
                    tossAndMatchWinners[tossWinner]++;
                } else {
                    tossAndMatchWinners[tossWinner] = 1;
                }
            }
        }

        fs.writeFileSync("../public/output/countOfTossAndMatchWinners.json", JSON.stringify(tossAndMatchWinners, null, 2),
            (err) => {
                if (err) {
                    console.log(err, "Error writing the file");
                } else {
                    console.log("Result written in the output");
                }
            }
        );
    });
}catch(error){
    console.log(error);
}
}
countOfTossAndMatchWinners();
