const fs = require("fs");
const csvTOJSON = require("../helper.js");

const matchesFilepath = "../data/matches.csv";
const deliveriesFilepath = "../data/deliveries.csv";

function extraRunsConcededPerTeamIn2016(){
   try{// let res={};
    csvTOJSON(matchesFilepath).then((yearDetails) =>{
        let matchID = [];
        for(let index =0;index<yearDetails.length;index++){
            if(yearDetails[index].season =="2016"){
                matchID.push(yearDetails[index].id);
            }
        }
 //       console.log(matchID);
        csvTOJSON(deliveriesFilepath).then((deliveryDetails) => {
        
            let extraRunsCOnceded = {};
            for(let index =0;index<deliveryDetails.length;index++){
                if(matchID.includes(deliveryDetails[index].match_id)){
                    if(!extraRunsCOnceded[deliveryDetails[index].bowling_team]){
                        extraRunsCOnceded[deliveryDetails[index].bowling_team]= Number(deliveryDetails[index].extra_runs);
                    }else{
                        extraRunsCOnceded[deliveryDetails[index].bowling_team]+= Number(deliveryDetails[index].extra_runs);
                    }
                }
                
            }
            fs.writeFileSync("../public/output/extraRunsConcededPerTeamIn2016.json", JSON.stringify(extraRunsCOnceded, null, 2), function(err) {
                if (err) {
                    console.log(err, "Error writing the file");
                } else {
                    console.log("Result written in the output");
                }
            });
        })
    })
}catch(error){
    console.log(error);
}
}
extraRunsConcededPerTeamIn2016();